include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include "GPIOClass.h"
#define DELAY 1

using namespace std;


//Modifies address byte to desired pin, possible to select channels 0-7
void changeChannel(int channel, int (*addressIn)[15])
{
	(*addressIn)[0] = 1;// Start Bit
	(*addressIn)[1] = 1;// Select Bit
	(*addressIn)[4] = inputN & 1;
	(*addressIn)[3] = (inputN >>1) & 1;
	(*addressIn)[2] = (inputN >>2) & 1;
	cout <<"Start Bit: " << (*addressIn)[0] <<" Type Bit: "<< (*addressIn)[1] << endl; 
	cout <<" D2: " << (*addressIn)[2] <<" D1: "<< (*addressIn)[3] << " D0: " << (*addressIn)[4]<<endl;
}


void writeOne(GPIOClass* pin, GPIOClass* Clk)
{
	pin->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	pin->setval_gpio("0");
	
}

void writeZero(GPIOClass* pin, GPIOClass* Clk)
{
	pin->setval_gpio("0");
	usleep(DELAY);
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	usleep(DELAY/2);
}


void clearAddress(GPIOClass* chipin, GPIOClass* Clk)
{
	cout << "Clear" << endl;
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	chipin->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	chipin->setval_gpio("0");

}


void sendAddress(GPIOClass* chipIn, GPIOClass* mosi, GPIOClass* Clk, int (*address)[8])
{
	
	clearAddress(chipIn,Clk);
	int bit = 0;	
	while(bit < 5)
	{
		if((*address)[bit] == 1)
		{
			writeOne(mosi,Clk);
		}
		else
		{
			writeZero(mosi,Clk);
		}
		bit++;
	}

}



int* readValue(GPIOClass* miso, GPIOClass* Clk)
{
	//Ignore first two bits returned
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	usleep(DELAY);
	Clk->setval_gpio("1");
	usleep(DELAY);
	Clk->setval_gpio("0");
	int intValue=0;
	int testval;
	int bitValue[10];
	int bitcount = 0;
	for(bitcount <=10; bitcount++)
	{
		miso->getval_gpio(testval); //MSB stored first
		cout << testval << endl;
		bitValue[bitcount]=testval;
		Clk->setval_gpio("1");
		usleep(DELAY);
		Clk->setval_gpio("0");
		usleep(DELAY);
		bitcount++;
	}
	for(int bit = 512;bit <=1; bit++)
	{		
		intValue = inValue+bitValue[bitcount];
		bit = bit/2;
		bitcount--;	
	}	 
	return *intValue;	


}



int main(void)
{
	struct time start, end;
	long mtime, seconds, useconds;
	
		

	GPIOClass* Clock = new GPIOClass("11");
	GPIOClass* MOSI = new GPIOClass("10");
	GPIOClass* MISO = new GPIOClass("9");
	GPIOClass* ChipEn = new GPIOClass("5");	
	
	cout << "Pins instatiated" << endl;
	
	int addressInput[5];

	Clock->export_gpio();
	MOSI->export_gpio();
	MISO->export_gpio();
	ChipEn->export_gpio();
		
	cout << "Pins exported" << endl;
	
	Clock->setdir_gpio("out");
	MOSI->setdir_gpio("out");
	MISO->setdir_gpio("in");
	ChipEn->setdir_gpio("out");

	cout << "Pins set" << endl;
	int x = 0;
	
	selectInput(1, &addressInput);
	sendAddress(ChipEn,MOSI,Clock,&addressInput);
    	x = readValue(MISO,Clock);
	cout << x <<endl;

}



